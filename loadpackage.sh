#!/bin/sh

PACKAGE=$1
IMAGE=$2/${PACKAGE}.pkg
MOUNT_DIR=$3/${PACKAGE}.pkg.d 

/sbin/mkdir -p ${MOUNT_DIR} || exit 1
/bin/mount -oro ${IMAGE} ${MOUNT_DIR} || exit 1
/sbin/mount.aufs -oremount,ins:1:${MOUNT_DIR}=rr / || exit 1
/usr/bin/test -e /usr/share/packages/${PACKAGE}/start && /usr/share/packages/${PACKAGE}/start
