#!/bin/sh

PACKAGE=$1
MOUNT_DIR=$2/${PACKAGE}.pkg.d 

/usr/bin/test -e /usr/share/packages/${PACKAGE}/stop && /usr/share/packages/${PACKAGE}/stop

/sbin/mount.aufs -oremount,del:${MOUNT_DIR} / || exit 1
/bin/umount ${MOUNT_DIR} || exit 1
/bin/rmdir ${MOUNT_DIR} || exit 1
