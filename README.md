Package System for a Live File-System
=====================================

About
-----

The packaging system is a series of systemd units and a generator that will merge in loop-back mounted images into an existing AUFS layer structure.  There is an assumption that layer 0 is the read-only file-system and the top layer is the read-write file-system.


Package Architecture
--------------------

A package is a loop-back image of a file-system that optionally contains two hook scripts for startup and shutdown of the package.  This allows things like services to be configured to run when the package is loaded.

![](load_process.svg "Load Process")

Building a Package
------------------